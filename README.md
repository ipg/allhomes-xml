AllhomesXml
===========

This class generates an XML file compatible with allhomes.com.au XML feed.  It is designed to make it easy to produce a compatible XML file from any data.

Installation
------------

    gem 'allhomes_xml', :git => "git@bitbucket.org:ipg/allhomes-xml.git"

Notes
-----

*  Please ensure all strings are XML compatible - use the String#to_xs method.

*  Ensure all line breaks are defined as <br> not <br />

*  Both sales and rental listings are automatically converted to commercial if the property type is one of AllhomesXml::Generator::COMMERCIAL_PROPERTY_TYPES

Listings
--------

When defining a listing you can define a SaleListing or RentalListing (see example below).  Valid attributes to pass in to either are:

*  id: String - An id unique to this property
*  property_type: String - House/Apartment/etc.  See the allhomes XSD for exact types
*  agency_id: Integer - The Allhomes ID of the agency this property is listed with
*  agents: AllhomesXML::Agents - See example below
*  unit_number: Integer - Unit/Apartment/Door number of property
*  street_number: Integer
*  street_name: String - Street name without the type
*  street_type: String - Street/Circuit/Way/etc.  See the allhomes XSD for exact types
*  complex: String - Name of the complex/building this property belongs to
*  suburb: String - See the allhomes XSD for options
*  postcode: Integer
*  state: String
*  country: String
*  hide_address: Boolean - Only display the suburb on allhomes, exclude unit and street information
*  title: String - Copy headline
*  description: String - Property copy
*  eer: Float
*  photos: AllhomesXML::Photos - See example below
*  exhibitions: AllhomesXML::Exhibitions - See example below
*  bedrooms: Integer - Number of Bedrooms
*  bathrooms: Integer - Number of Bathrooms 
*  ensuites: Integer - Number of Ensuites
*  carspaces: Integer - Number of Carspaces
*  garagespaces: Integer - Number of Garage spaces
*  attributes: AllhomesXML::Attributes - See example below

SaleListing specific
--------------------

*  price: AllhomesXML::Price - See example below for more options (Pass in nil for 'By Negotiation')
*  exchange_date: Date - Required for sold properties
*  sale_price: Integer - Required for sold properties
*  offers_over: Boolean
*  floor_area: Float - Size of house
*  block_size: Float - Size of land
*  core_area: Boolean
*  body_corporate: Boolean - Property is part of a body corporate
*  plans_approved: Boolean
*  block: Integer
*  section: Integer
*  under_offer: Boolean
*  sold: Boolean
*  uv: Integer
*  withdrawn: Boolean

RentalListing specific
----------------------

*  price: Integer - Per week
*  available_from: Date
*  available_to: Date
*  pets_allowed: Boolean
*  short_term_lease: Boolean
*  rented: Boolean
*  rented_date: Date - Required for rented properties
*  rented_price: Integer - Required for rented properties (Rent is per week)
*  withdrawn: Boolean
*  under_application: Boolean

Validation
----------

A validator is built in allowing you to validate the XML you produce:

    errors = AllhomesXml::Validator.validate xml_string
    
    if errors.blank?
      puts "Validation Successful"
    else
      errors.each { |e| puts e }
    end

Example
-------

    # Begin by creating a new allhomes_xml generator
    allhomes_xml = AllhomesXml::Generator.new :username => 'allhomes_xml_username', :password => 'allhomes_xml_password'
  
    # Create a class to house the price (See price section above for more options)
    sale_listing_price = AllhomesXml::Price::Fixed.new :price => 999999
    # Other options are:
    # price = AllhomesXml::Price::Auction.new :date => Date.today, :start => Time.now, :estimate => 999999
    # price = AllhomesXml::Price::Tender.new :date => Date.today
    # price = AllhomesXml::Price::EOI.new :date => Date.today
    # price = AllhomesXml::Price::Range.new :low => 888888, :high => 999999
  
    # Create a class to house the photos
    sale_listing_photos = AllhomesXml::Photos.new
    sale_listing_photos << {
      :url =>  "http://www.yourwebsite.com.au/photo.jpg",
      :caption => "Backyard",
      :order => 1
    }
                          
    # Create a class to house the exhibitions
    sale_listing_exhibitions = AllhomesXml::Exhibitions.new 
    sale_listing_exhibitions << {
      :date => Date.today,
      :start => Time.now,
      :finish => Time.now + 3600
    }
                                
    # Create a class to house listing attributes
    sale_listing_attributes = AllhomesXml::Attributes.new
    # These key/value pairs correspond to the pre-defined options in the allhomes.xsd
    sale_listing_attributes["airConditioning"] = "reverseCycle"
  
    # Create a class to house the agent details
    sale_listing_agents = AllhomesXml::Agents.new
    sale_listing_agents.primary :firstname => "Joe", :surname => "Bloggs"
    # Second agent is optional
    sale_listing_agents.secondary :firstname => "Joe", :surname => "Bloggs"
  
    # Create a new sale listing
    sale_listing = AllhomesXml::SaleListing.new(
  		:property_type => "House",
  		:agency_id => 1,
  		:unit_number => 1,
  		:street_number => 2,
  		:street_name => "Test",
  		:street_type => "Street",
  		:complex => "Lakeview",
  		:suburb => "Turner",
  		:postcode => 2612,
  		:state => "ACT",
  		:country => "Australia",
  		:hide_address => false,
  		:title => "Fantastic Property",
  		:description => "This wonderful property...",
  		:eer => 3.5,
  		:bedrooms => 4,
  		:bathrooms => 2,
  		:ensuites => 1,
  		:carspaces => 0,
  		:garagespaces => 2,
  		:price => sale_listing_price,
  		:photos => sale_listing_photos,
  		:exhibitions => sale_listing_exhibitions,
  		:attributes => sale_listing_attributes,
  		:agents => sale_listing_agents
  	)
  	
    # Add the sale listing to the XML
    allhomes_xml << sale_listing
  	
    # Generate the XML
    puts allhomes_xml.generate
