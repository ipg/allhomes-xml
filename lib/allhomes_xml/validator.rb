module AllhomesXml
  
  class Validator
  
    # Validates the XML
    def self.validate xml_string
  
      # Include LibXML for parsing the XML
      require 'xml'
  
      # This tells LibXML to store any errors encoutered in the ERRORS variable
      errors = []
      LibXML::XML::Error.set_handler { |error| errors.push error.to_s }
  
      document = LibXML::XML::Document.string xml_string
      schema = LibXML::XML::Schema.new(File.dirname(__FILE__) + '/../allhomes.xsd')
  
      begin
  
        # Validate the XML
        document.validate_schema schema
        return []
  
      # If rescue is triggered, then there were errors and we need to deliver the email
      rescue LibXML::XML::Error => e
  
        puts "--Errors Encountered:"
        return errors
  
      end
  
    end
  
  end
  
end