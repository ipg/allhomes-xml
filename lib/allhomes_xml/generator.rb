module AllhomesXml
  
  class Generator
    
    COMMERCIAL_PROPERTY_TYPES = %w(office hotel industrial motel retail warehouse)

    # Creates a new XML class to house all data
    # Takes an Allhomes username and password hash as it's argument
    def initialize(args)
  
      @username = args[:username]
      @password = args[:password]
      @properties = []
  
    end
    
    def << property
      @properties << property 
    end

    # Generates the XML
    def generate
  
      require 'builder'

      xml = Builder::XmlMarkup.new :indent => 4
  
      xml.instruct! :xml, :version => "1.0"

      time = Time.now
      xml.listings({:feedId => time.to_i,
                    :generated => time.xmlschema,
                    :username => @username,
                    :password => @password,
                    :xmlns => "http://www.allhomes.com.au/2008/v1/listingFeed",
                    :'xmlns:xsi' => "http://www.w3.org/2001/XMLSchema-instance",
                    :'xsi:schemaLocation' => "http://www.allhomes.com.au/2008/v1/listingFeed listingFeed.xsd "}) do
                        

        @properties.each do |property|
        
          # Set up a quick variable for checking if this property is a sale listing
          property.class.name == "AllhomesXml::SaleListing" ? sale_listing = true : sale_listing = false
        
          if sale_listing
        
            # Attributes for the initial tag
            listingAttributes = {   :saleStatus => get_sale_status(property),
                                    :uniqueId => "S#{property.id}"}

            # If this property is sold, add the attributes to the initial tag
            if property.exchange_date and property.sale_price
              listingAttributes.update({:saleDate => property.exchange_date.strftime("%Y-%m-%d"), :salePrice => property.sale_price})
            end
          
          else
          
            # Attributes for the initial tag
            listingAttributes = {  :rentalStatus => get_rent_status(property),
                                   :uniqueId => "R#{property.id}"}

            # If the property is rented, add the rented date and price
            if property.rented and property.rented_date and property.rented_price
              listingAttributes.update({:rentedDate => property.rented_date.strftime("%Y-%m-%d"), :rentedPrice => property.rented_price})
            end
          
          end
        
          # Generate initial tag
          if COMMERCIAL_PROPERTY_TYPES.include? property.property_type
            sale_listing ? opening_tag = "commercialSaleListing" : opening_tag = "commercialRentalListing"
          else
            sale_listing ? opening_tag = "residentialSaleListing" : opening_tag = "residentialRentalListing"
          end

          # Opening Tag
          ######################################################
          xml.tag!(opening_tag, listingAttributes) do
          
            # Agent Details
            ######################################################
            xml.agentDetails do
              xml.agencyId property.agency_id
              xml.primaryContact do
                xml.firstName property.agents.primary_agent[:firstname]
                xml.lastName property.agents.primary_agent[:surname]
              end
          
              if sale_listing
                if property.agents.secondary_agent
                  xml.alternativeContact do
                    xml.firstName property.agents.secondary_agent[:firstname]
                    xml.lastName property.agents.secondary_agent[:surname]
                  end
                end
              end
          
            end

            # Street Address
            ######################################################
            xml.streetAddress do

              xml.unitNumber property.unit_number unless property.unit_number.blank?
              xml.streetNumber property.street_number unless (property.street_number =~ /^[&\w\- ]+$/).nil?
              xml.streetName property.street_name
              xml.streetType property.street_type
              xml.propertyName property.complex unless property.complex.blank?
              xml.suburb property.suburb
              xml.postCode property.postcode
              xml.state property.state
              xml.country "Australia"

              if property.hide_address
                xml.addressVisibility "HIDE_STREET_NUMBER_AND_NAME"
              else
                xml.addressVisibility "VISIBLE"
              end

            end

            # Formal Address - Sale Only
            ######################################################
            if sale_listing and property.state == 'ACT'

              xml.formalAddress do

                  xml.blockSection do

                    if (property.block =~ /[^0-9]/).nil?
                      xml.block property.block
                    else
                      xml.block
                    end
                    xml.section property.section

                  end

                # end

              end

            end

            # Title and Description
            ######################################################
            xml.title property.title
            if property.description.blank?
              xml.description 'No description available.'
            else
              xml.description property.description
            end

            # EER
            ######################################################
            xml.eer property.eer if not property.eer.blank? and not property.eer == 'N/A'

            # Street Directory Reference
            ######################################################
            xml.streetDirectoryReference do

              xml.type ""
              xml.reference ""    

            end

            # Listing Status
            ######################################################
            xml.listingStatus "active"
          
            # Photos
            ######################################################
            unless property.photos.blank?
            
              xml.images do

                property.photos.each do |photo|

                  xml.image do

                    xml.imageReference photo[:url]
                    xml.caption photo[:caption] unless photo[:caption].blank?
                    xml.orderIndex photo[:order]

                  end

                end

              end
            
            end
          
            # Rental Availability - Rentals Only
            ######################################################
            if not sale_listing and (property.available_from or property.available_to)

              xml.availability do

                xml.from property.available_from.strftime("%Y-%m-%d") if property.available_from
                xml.to property.available_to.strftime("%Y-%m-%d") if property.available_to

              end

            end
          
            # Sale Authority - Sale Only
            ######################################################
            xml.saleAuthority "exclusive" if sale_listing
          
            # Price
            ######################################################
            if sale_listing
            
              case property.price.class.name
          
                # If the property is an auction
                when "AllhomesXml::Price::Auction"

                  xml.auction do

                    xml.date property.price.date.strftime("%Y-%m-%d")
                    xml.startTime property.price.start.strftime("%H:%M:%S")
                    xml.unpublishedPriceEstimate property.price.estimate unless property.price.estimate.blank?
                  
                  end

                # If the property is by tender
                when "AllhomesXml::Price::Tender"

                  xml.tender do

                    xml.closingDate property.price.date.xmlschema

                  end

                # If the property is by Expressions of Interest
                when "AllhomesXml::Price::EOI"

                  xml.expressionOfInterest do

                    xml.closingDate property.price.date.xmlschema    

                  end

                # If the property is sold by plain old price
                when "AllhomesXml::Price::Range"

                  xml.price do

                    xml.lower property.price.low
                    xml.upper property.price.high
                  
                  end
                
                when "AllhomesXml::Price::Fixed"
                
                  xml.price do

                    xml.lower property.price.price
                    property.offers_over ? xml.offersAbove("true") : xml.offersAbove("false")
                  
                  end
            
                # If the property is by negotiation
                else

                  xml.price ""
              
              end
            
            # If rental listing
            else
            
              xml.price do

                xml.lower property.price unless property.price.blank?
                xml.paymentPeriod "weekly" unless property.price.blank?

              end
            
            end
          
            # Exhibitions
            ##################
            unless property.exhibitions.blank?
                        
              xml.exhibitions do

                property.exhibitions.each do |opentime|

                  xml.exhibition do

                    xml.date opentime[:date].strftime("%Y-%m-%d")
                    xml.startTime opentime[:start].strftime("%H:%M:%S")
                    xml.endTime opentime[:finish].strftime("%H:%M:%S")

                  end

                end
            
              end
            
            end
          
            # Rental Purpose
            ##################
            xml.rentalPurpose 'residential' if not sale_listing and not COMMERCIAL_PROPERTY_TYPES.include?(property.property_type)
          
            # Property Details
            ##################
            xml.propertyDetails do

              # House & Block Size
              ##################
              xml.buildingDetails do

                if sale_listing and property.floor_area and not property.floor_area.to_f == 0

                  xml.buildingSize property.floor_area.to_f
                
                end

              end

              xml.landDetails do

                xml.blockSize property.block_size if sale_listing and property.block_size and (property.block_size =~ /[^0-9.]/).nil?

              end

              # UV
              ##################
              xml.unimprovedLandValue(property.uv) if sale_listing and not property.uv.blank?

              # Property Type
              ##################
              xml.propertyType property.property_type

              # Features
              ##################
              # If we're not dealing with a commercial property, output the features
              unless COMMERCIAL_PROPERTY_TYPES.include? property.property_type

                # Features unique to sales / rentals
                features = {}
                if sale_listing
                  features['coreArea'] = true if property.core_area
                  features['bodyCorporate'] = true if property.body_corporate
                  features['plansApproved'] = true if property.plans_approved
                else
                  features['petsAllowed'] = true if property.pets_allowed
                  features['shortTermLease'] = true if property.short_term_lease
                end

                # Features common to both sales / rentals
                features[:bedrooms] = property.bedrooms unless property.bedrooms.blank?
                features[:bathrooms] = property.bathrooms unless property.bathrooms.blank?
                features[:ensuites] = property.ensuites unless property.ensuites.blank?
                features[:carportSpaces] = property.carspaces unless property.carspaces.blank?
                features[:garageSpaces] = property.garagespaces unless property.garagespaces.blank?
                
                # Merge extra attributes
                features.update property.attributes

                # Features tag
                xml.features features

              end

            end

            xml.outboundEndpoints do
              xml.outboundEndpoint do
                xml.endpointName 'domain.com.au'
                xml.outboundEnabled 'true'
              end
              if property.realestate_com_au
                xml.outboundEndpoint do
                  xml.endpointName 'realestate.com.au'
                  xml.outboundEnabled 'true'
                end
              end
            end
          
          end
        
        end

      end
  
    end
  
    protected
  
    def get_sale_status property
    
      if property.sold
        'sold'
      else
        if property.withdrawn
          'withdrawn'
        elsif property.under_offer
          'underOffer'
        else
          'forSale'
        end
      end
    
    end
  
    def get_rent_status property
    
      if property.rented
        'rented'
      else
        if property.withdrawn
          'withdrawn'
        elsif property.under_application
          'underApplication'
        else
          'forRent'
        end
      end
    
    end

  end
  
end