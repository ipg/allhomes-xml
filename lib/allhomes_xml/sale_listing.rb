module AllhomesXml

  # Creates a sale listing that is then parsed by AllhomesXML::XML to generate the XML
  class SaleListing < BasicListing

    # Attributes for a sale listing
    attr_accessor :exchange_date, :sale_price, :offers_over, :floor_area, :block_size, :core_area, :body_corporate, :plans_approved, :block, :section, :under_offer, :sold, :uv, :withdrawn

  end
  
end