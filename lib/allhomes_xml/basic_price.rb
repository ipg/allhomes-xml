module AllhomesXml
  
  module Price
  
    class BasicPrice
      def initialize(args)
        args.each_pair do |attribute, value|
          send "#{attribute}=", value
        end
      end
    end
  
  end
  
end