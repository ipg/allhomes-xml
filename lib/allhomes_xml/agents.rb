module AllhomesXml

  # Storage container for agents - primary and secondary(optional)
  class Agents

    attr_accessor :primary_agent, :secondary_agent

    def primary hash
      self.primary_agent = hash
    end
    def secondary hash
      self.secondary_agent = hash
    end

  end
  
end