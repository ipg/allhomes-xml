module AllhomesXml

  # Creates a template listing for use by higher level classes
  class BasicListing

    # All generic attributes common to all listings
    attr_accessor :id, :property_type, :agency_id, :agents, :unit_number, :street_number, :street_name, :street_type, :complex, :suburb, :postcode, :state, :country, :hide_address, :title, :description, :eer, :photos, :price, :exhibitions, :bedrooms, :bathrooms, :ensuites, :carspaces, :garagespaces, :attributes

    def required
      [:id, :agents, :suburb, :property_type, :postcode]
    end

    # Creates a new basic listing - takes a hash full of attributes as an argument
    def initialize(args)

      args.each_pair do |attribute, value|

        # Remove junk off the beginning/end of strings
        value.strip! if value.instance_of? String and not attribute == :description

        send "#{attribute}=", value

      end

      required.each { |field| raise "#{field} field cannot be blank - property #{id}!" if self.send(field).blank? }

    end

  end

end