module AllhomesXml

  class RentalListing < BasicListing

    # Attributes for a rental listing
    attr_accessor :available_from, :available_to, :pets_allowed, :short_term_lease, :rented, :rented_date, :rented_price, :withdrawn, :under_application, :realestate_com_au

  end
  
end