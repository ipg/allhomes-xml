# Require all other files
require 'allhomes_xml/basic_listing.rb'

Dir[File.dirname(__FILE__) + '/allhomes_xml/*.rb'].each {|file| require file }

# Require price files
Dir[File.dirname(__FILE__) + '/allhomes_xml/price/*.rb'].each {|file| require file }